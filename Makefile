CC = g++
FILES = fuelMonitor.cpp
OUT_EXE = fuelMonitor

build: $(FILES)
	$(CC) -o $(OUT_EXE) -I/usr/local/include -I/usr/local/include/cppconn -I/usr/local/lib -lmysqlcppconn $(FILES)

clean:
	rm -f *.o core
