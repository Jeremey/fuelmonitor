/* socat PTY,link=./virtual-tty,raw,echo=0 -
 *
 * \file      Example1.cpp

 \brief     Example code source for class serialib.
            This example open the device on ttyACM0.
            (USB to RS232 converter under linux).
            If the opening is successful, it sends the AT command
            and waits for a string being received from the device.
            After 5 seconds, if no valid data are received from the
            device, reception is giving up.

 \author    Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
 \version   1.2
 \date      05/01/2011
 */

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <stdexcept>
//#include "serialib.h"
#include "serialib.cpp"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#define url "localhost"
#define user "root"
#define pass "Freidn0r"
#define database "fuelMonitor"

using namespace std;

//#define         DEVICE_PORT             "/dev/ttyS0"                         // ttyS0 for linux
/*#define DEVICE_PORT "virtual-tty"         // ttyS0 for linux*/
#define DEVICE_PORT "/dev/ttyACM0" // ttyS0 for linux


int main()
{
    serialib LS;                            // Object of the serialib class
    int Ret;                                // Used for return values
    char Buffer[128];

    // Open serial port
    Ret=LS.Open(DEVICE_PORT,9600);          // Open serial link at 115200 bauds
    if (Ret!=1) {                           // If an error occured...
        printf ("Error while opening port. Permission problem ?\n");        // ... display a message ...
        return Ret;                         // ... quit the application
    }
    printf ("Serial port opened successfully !\n");

    // Write the AT command on the serial port
    /*Ret=LS.WriteString("AT\n");             // Send the command on the serial port
    if (Ret!=1) {                           // If the writing operation failed ...
        printf ("Error while writing data\n"); // ... display a message ...
        return Ret;                         // ... quit the application.
    }
    printf ("Write operation is successful \n");*/

	string boilerState;
	string sqlQuery;


	while (1) {
		// Wait for port activity
		int fd = open("/dev/ttyACM0", O_RDONLY);
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		/*struct timeval timeout = { NULL, NULL };*/
		int ret = select(fd+1, &fds, NULL, NULL, NULL); // No timeout


		// Read a string from the serial device
		// Read a maximum of 128 characters with a timeout of 5 seconds
		Ret=LS.ReadString(Buffer,'\n',128,0);
		// The final character of the string must be a line feed ('\n')
		// If a string has been read from, print the string



		if (Ret>0) {

			sql::Driver* driver = get_driver_instance();
			std::auto_ptr<sql::Connection> con(driver->connect(url, user, pass));
			con->setSchema(database);
			std::auto_ptr<sql::Statement> stmt(con->createStatement());

			sqlQuery = std::string("INSERT into boiler(state) VALUES('") +Buffer+ std::string("')");
			cout << sqlQuery;

			stmt->execute(sqlQuery);

			sleep(1);

		// Close the connection with the device
		}
		//printf("1\n");
	}

	LS.Close();

    return 0;
}
