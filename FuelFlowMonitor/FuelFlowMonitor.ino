#include <Arduino.h>
#include "Switch.h"



const byte toggleSwitchpin = 3; // (right button)
const byte buttonGNDpin = 4; // (left button)
const byte ButtonVCCpin = 6; 
const byte Button10mspin = 8; 
const int ledPin =  13;
int i;

Switch buttonGND = Switch(buttonGNDpin); // button to GND, use internal 20K pullup resistor
Switch toggleSwitch = Switch(toggleSwitchpin); 
Switch buttonVCC = Switch(ButtonVCCpin, INPUT, HIGH); // button to VCC, 10k pull-down resistor, no internal pull-up resistor, HIGH polarity
Switch button10ms = Switch(Button10mspin, INPUT_PULLUP, LOW, 1); // debounceTime 1ms

void setup() {
	Serial.begin(9600);
}

void loop() {
	buttonGND.poll();  
	if(toggleSwitch.poll()) {
		if (toggleSwitch.on() == 1) {

			Serial.println(1); 
		} else if (toggleSwitch.on() == 0) {

			Serial.println(0);
		}


	}

}

